/**
* 2015110705 김민규
* 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 작성하였습니다.
*/

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

#define MAX_ELEMENTS 200 /* maximum heap size + 1 */
#define HEAP_FULL(n) (n == MAX_ELEMENTS - 1)
#define HEAP_EMPTY(n) (!n)

typedef struct {
	int key;
	/* other fields */
} Element;

Element heap[MAX_ELEMENTS];
int n = 0;

void push(Element item, int *n);
Element pop(int* n);

int main()
{
	FILE* inputFile = fopen("input.txt", "r");
	int buffer;
	int i;

	printf("***** insertion into a min heap ***** \n");
	while (!feof(inputFile)) {
		Element newItem;
		fscanf(inputFile, "%d", &newItem.key);
		push(newItem, &n);
		for (i = 1; i <= n; i++) {
			printf("%2d ", heap[i].key);
		}
		printf("\n");
	}

	printf("***** deletion from a min heap ***** \n");
	while (n > 0) {
		pop(&n);
		for (i = 1; i <= n; i++) {
			printf("%2d ", heap[i].key);
		}
		printf("\n");
	}

	fclose(inputFile);

	return 0;
}


/**
* insert item into a max heap of current size *n
* @param	: insert item
* @param	: current size *n
*/
void push(Element item, int *n)
{
	int i;
	if (HEAP_FULL(*n)) {
		fprintf(stderr, "The heap is full. \n");
		exit(EXIT_FAILURE);
	}
	i = ++(*n);
	while ((i != 1) && (item.key < heap[i / 2].key)) {
		heap[i] = heap[i / 2];
		i /= 2;
	}
	heap[i] = item;
}

/**
* delete element with the highest key from the heap
* @param	: current size *n
*/
Element pop(int* n)
{
	int parent, child;
	Element item, temp;

	if (HEAP_EMPTY(*n)) {
		fprintf(stderr, "The heap is empty \n");
		exit(EXIT_FAILURE);
	}

	/* save value of the element with the lowest key */
	item = heap[1];

	/* use last element in heap to adjust heap */
	temp = heap[(*n)--];
	parent = 1;
	child = 2;
	while (child <= *n) {
		/* find the smaller child of the current parent */
		if ((child < *n) && (heap[child].key > heap[child + 1].key)) {
			child++;
		}
		if (temp.key <= heap[child].key) {
			break;
		}

		/* move to the next lower level */
		heap[parent] = heap[child];
		parent = child;
		child *= 2;
	}
	heap[parent] = temp;
	return item;
}