/**
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
//#define COMPARE(x, y) (((x)<(y)) ? -1 : ((x)==(y)) ? 0 : 1)
#define MALLOC(p, s) \
	if( !( (p) = malloc( s ) ) ){	\
		fprintf(stderr, "Insufficient memory");\
		exit(EXIT_FAILURE);\
		}
#define MAX_STACK_SIZE 100

// tree node & header
typedef struct node *treePointer;
typedef struct node
{
	char data;		// operator or operand in char type
	treePointer leftChild, rightChild;
}node;
treePointer root = NULL;

// stack
treePointer stack[MAX_STACK_SIZE];
int top = -1;
void push(treePointer item);
void stackFull();
treePointer pop();
treePointer stackEmpty();

// postfix expression 
char expr[81];

// binary tree of an arithmetic expression
typedef enum {
	lparen, rparen, plus, minus, times, divide,
	mod, and, or, not, eos, operand
} precedence;
precedence getToken(char *symbol, int *n);
void createBinTree();

// binary tree traversals
void inorder(treePointer ptr);

int main(void)
{
	int result;
	FILE *fp;

	if ((fp = fopen("input.txt", "r")) == NULL)
	{
		fprintf(stderr, "cannot open the file");
		exit(EXIT_FAILURE);
	}

	printf("the length of input string should be less than 80\n");
	fgets(expr, 80, fp);
	printf("input string (postfix expression) : %s\n", expr);

	printf("creating its binary tree\n\n");
	createBinTree();

	printf("%-20s : ", "inorder traversal");
	inorder(root); 	printf("\n");

	return 0;
}

// create a binary tree from a postfix arithmetic expression
void createBinTree()
{
	/* scanning the expr from left to right */
	precedence	token;
	char		symbol;
	int			n = 0; 

	token = getToken(&symbol, &n);
	while (token != eos) {
		/* if token is operand */
		if (token == operand) {
			/* make new node (*new node's both links are NULL)*/
			treePointer newNode = (treePointer)malloc(sizeof(node));
			newNode->leftChild = NULL;
			newNode->rightChild = NULL;
			newNode->data = symbol;

			/* push to stack */
			push(newNode);
		}
		/* if token is not */
		else if (token == not) {
			/* make new node */
			treePointer newNode = (treePointer)malloc(sizeof(node));

			/* left child - NULL*/
			newNode->leftChild = NULL;

			/* right child - pop*/
			newNode->rightChild = pop();

			/* push operator node to stack */
			newNode->data = symbol;

			push(newNode);
		}
		/* if token is operator */
		else {
			/* make new node */
			treePointer newNode = (treePointer)malloc(sizeof(node));

			/* right child - pop node */
			newNode->rightChild = pop();

			/* left child - pop node */
			newNode->leftChild = pop();

			/* push operator node to stack */
			newNode->data = symbol;

			push(newNode);
		}
		root = stack[top];
		token = getToken(&symbol, &n);
	}
}

// Program 3.14: Function to get a token from the input string
precedence getToken(char *symbol, int *n)
{ /* get the next token, symbol is the character representation,
  whichis returned, the tokenis represented by its enumerated value,
  which is returned inthe function name */
	*symbol = expr[(*n)++];
	switch (*symbol)
	{
		//case '('		: return lparen;
		//case ')'		:  return rparen;
	case '+': return plus;
	case '-': return minus;
	case '/': return divide;
	case '*': return times;
	case '%': return mod;
	case '|': return or;
	case '~': return not;
	case '&': return and;
	case '\0': return eos;
	default: return operand; /* no error checking, default is operand */
	}
}

///////////////////// binary tree traversals //////////////////////////////

// Program 5.1 : Inorder traversal of a binary tree
void inorder(treePointer ptr)
{
	if (ptr)
	{
		inorder(ptr->leftChild);
		printf("%c", ptr->data);
		inorder(ptr->rightChild);
	}
}

/////////////////////// stack operations ///////////////////////////////////
treePointer pop()
{/* delete and return the top int from the stack */
	if (top == -1)
		return stackEmpty();	/* returns an error key */
	return stack[top--];
}

treePointer stackEmpty()
{
	treePointer item;
	item = NULL;  // error key value - null pointer
	return item;
}

void push(treePointer item)
{/* add an item to the global stack */
	if (top >= MAX_STACK_SIZE - 1)
		stackFull();
	stack[++top] = item;
}

void stackFull()
{
	fprintf(stderr, "stack is full, cannot add item\n");
	exit(EXIT_FAILURE);
}
